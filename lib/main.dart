import 'package:azie_pay/views/on-boarding-child1.dart';
import 'package:azie_pay/views/on-boarding-child2.dart';
import 'package:azie_pay/views/on-boarding.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: MaterialColor(
          0xFF175388,
          <int, Color>{
            50: Color(0xEE175388),
            100: Color(0xDD175388),
            200: Color(0xBB175388),
            300: Color(0xAA175388),
            400: Color(0x99175388),
            500: Color(0x88175388),
            600: Color(0x77175388),
            700: Color(0x66175388),
            800: Color(0x44175388),
            900: Color(0x22175388),
          },
        ),
        accentColor: Color(0xFF39B549),
        textTheme: TextTheme(
          bodyText1: TextStyle(color: Color(0xFF000000), fontSize: 14),
          bodyText2: TextStyle(color: Color(0xFF000000), fontSize: 12),
          headline1: TextStyle(color: Color(0xFF000000), fontSize: 32),
          headline2: TextStyle(color: Color(0xFF000000), fontSize: 28),
          headline3: TextStyle(color: Color(0xff000000), fontSize: 26),
          headline4: TextStyle(color: Color(0xff000000), fontSize: 24),
          headline5: TextStyle(color: Color(0xff000000), fontSize: 22),
          headline6: TextStyle(color: Color(0xff000000), fontSize: 20),
          subtitle1: TextStyle(color: Color(0xFF000000), fontSize: 18),
          subtitle2: TextStyle(color: Color(0xff000000), fontSize: 16),
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: OnBoarding(),
    );
  }
}
