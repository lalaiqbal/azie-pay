import 'package:flutter/material.dart';
import 'package:flutter/material.dart';

class DrawerListItem {
  String name;
  IconData icon;
  String routeName;
  DrawerListItem({this.name, this.icon, this.routeName});
}

class DrawerList {
  DrawerListItem dashboard = new DrawerListItem(
    name: 'Dashboard',
    icon: Icons.dashboard,
  );
  DrawerListItem myBooking = new DrawerListItem(
    name: 'My Booking',
    icon: Icons.calendar_today_outlined,
  );
  DrawerListItem myProfile = new DrawerListItem(
    name: 'My profile',
    icon: Icons.person,
  );
  DrawerListItem changePassword = new DrawerListItem(
    name: 'Change Password',
    icon: Icons.vpn_key,
  );
  DrawerListItem termsAndConditions = new DrawerListItem(
    name: 'Terms & Conditions',
    icon: Icons.copy,
  );
  DrawerListItem aboutUs = new DrawerListItem(
    name: 'About Us',
    icon: Icons.album_outlined,
  );
  DrawerListItem logOut =
      new DrawerListItem(name: 'Log out', icon: Icons.logout);

  List<DrawerListItem> drawerList = [];
  DrawerList() {
    this.drawerList.add(this.dashboard);
    this.drawerList.add(this.myBooking);
    this.drawerList.add(this.myProfile);
    this.drawerList.add(this.changePassword);
    this.drawerList.add(this.termsAndConditions);
    this.drawerList.add(this.aboutUs);
    this.drawerList.add(this.logOut);
  }
}
