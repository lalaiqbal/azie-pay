import 'package:flutter/cupertino.dart';

class TextInfo {
  String bodyText;
  String heading;
  TextInfo({@required this.bodyText, @required this.heading});
}

class OnBoardingMessage {
  List<TextInfo> messageList = [
    TextInfo(
      bodyText:
          'An instant and secure way to accept card payment, save time by not dealing with magnetic strips or chip readers',
      heading: 'Accept Credit/Debit Card Paymeny',
    ),
    TextInfo(
      bodyText:
          'Instantly capture card details by taking picture through you mobile camera',
      heading: 'Get Card Details Via Scan',
    ),
    TextInfo(
      bodyText: 'Add your card details manually as an alternate option',
      heading: 'Manually Add Card Details',
    ),
  ];
}
