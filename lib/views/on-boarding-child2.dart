import 'package:azie_pay/views/login.dart';
import 'package:azie_pay/views/shared/bigButtonFilled.dart';
import 'package:azie_pay/views/shared/materialChildButton.dart';
import 'package:azie_pay/views/snippets.dart';
import 'package:azie_pay/views/utils.dart';
import 'package:flutter/material.dart';

import 'constants/on-boarding-messages.dart';

class OnBoarding2 extends StatefulWidget {
  OnBoarding2({Key key}) : super(key: key);

  @override
  _OnBoarding2State createState() => _OnBoarding2State();
}

class _OnBoarding2State extends State<OnBoarding2> {
  Key key;
  int presentPage = 0;
  OnBoardingMessage messageList = new OnBoardingMessage();
  PageController pageController = new PageController();

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    this.pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Stay Conntected',
                    style: Theme.of(context).textTheme.headline3.copyWith(
                        color: Colors.white, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Please connect with your AziePay Account',
                    style: Theme.of(context).textTheme.subtitle2.copyWith(
                        color: Colors.white.withOpacity(0.8),
                        fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(mainBorder),
                        topRight: Radius.circular(mainBorder))),
                child: SizedBox(
                  width: double.infinity,
                  child: Column(
                    children: [
                      Expanded(
                        flex: 2,
                        child: imageContainer(
                            imagePath: 'assets/images/puzzle.png'),
                      ),
                      Expanded(
                          child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                left: mainPadding, right: mainPadding),
                            child: Column(
                              children: [
                                BigFilledButton(
                                    buttonText: 'Connect with AziePay',
                                    onPress: () {
                                      pop(context);
                                      push(context, Login());
                                    }),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('Dont have an account? ',
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1
                                            .copyWith(
                                                fontWeight: FontWeight.normal,
                                                color: Color(0XFF2E2E2E))),
                                    MaterialButton(
                                      minWidth: 0,
                                      padding: EdgeInsets.zero,
                                      materialTapTargetSize:
                                          MaterialTapTargetSize.shrinkWrap,
                                      onPressed: () {},
                                      child: Text('Register',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .copyWith(
                                                  color: Theme.of(context)
                                                      .accentColor)),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                              child: Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                                color: Color(0XFFE2E2E2),
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(mainBorder),
                                    topRight: Radius.circular(mainBorder))),
                            child: Padding(
                              padding: EdgeInsets.only(
                                  bottom: mainPadding + 15,
                                  left: mainPadding,
                                  right: mainPadding,
                                  top: mainPadding),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('FAQ and Support',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          .copyWith(
                                            fontWeight: FontWeight.w600,
                                            color:
                                                Theme.of(context).primaryColor,
                                          )),
                                  Text(
                                      'Do you have any queries? We are happy to help you!',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          .copyWith(
                                            fontWeight: FontWeight.w400,
                                          ))
                                ],
                              ),
                            ),
                          ))
                        ],
                      ))
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget imageContainer({String imagePath}) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(50),
        child: Image.asset(imagePath),
      ),
    );
  }
}
