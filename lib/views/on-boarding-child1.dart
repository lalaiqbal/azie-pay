import 'package:azie_pay/views/shared/materialChildButton.dart';
import 'package:azie_pay/views/utils.dart';
import 'package:flutter/material.dart';
import 'utils.dart';
import 'constants/on-boarding-messages.dart';

class OnBoarding1 extends StatefulWidget {
  Function jumpFunction;
  OnBoarding1({Key key, this.jumpFunction}) : super(key: key);

  @override
  _OnBoarding1State createState() => _OnBoarding1State();
}

class _OnBoarding1State extends State<OnBoarding1> {
  Key key;
  int presentPage = 0;
  OnBoardingMessage messageList = new OnBoardingMessage();
  PageController pageController = new PageController();

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    this.pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.48,
            child: PageView(
              controller: this.pageController,
              onPageChanged: (value) {
                setState(() {
                  this.presentPage = value;
                });
              },
              children: [
                this.imageContainer(
                    imagePath: 'assets/images/Illustration 1.png'),
                this.imageContainer(
                    imagePath: 'assets/images/Illustration 2.png'),
                this.imageContainer(
                    imagePath: 'assets/images/Illustration 3.png')
              ],
            ),
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(mainBorder),
                      topRight: Radius.circular(mainBorder))),
              child: Padding(
                padding: EdgeInsets.only(bottom: mainPadding, top: mainPadding),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                        flex: 2,
                        child: TextContainer(
                          bodyText:
                              messageList.messageList[presentPage].bodyText,
                          heading: messageList.messageList[presentPage].heading,
                          key: Key(this.presentPage.toString()),
                        )),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ...[0, 1, 2].map((data) => dotButton(context,
                                  index: data, presentPage: this.presentPage))
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                height: 50,
                                child: MaterialChildButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(25),
                                          bottomRight: Radius.circular(25))),
                                  onPress: () {},
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        left: mainPadding,
                                        right: mainPadding + 15),
                                    child: Text(
                                      'Skip',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          .copyWith(
                                              fontWeight: FontWeight.w400,
                                              color: Color(0XFFA6A6A6)),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 50,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      primary: Theme.of(context).accentColor,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(25),
                                              bottomLeft:
                                                  Radius.circular(25)))),
                                  onPressed: () {
                                    // todo remove this

                                    if (this.presentPage < 2) {
                                      setState(() {
                                        pageController.animateToPage(
                                          this.presentPage + 1,
                                          duration:
                                              const Duration(milliseconds: 350),
                                          curve: Curves.easeInOut,
                                        );
                                      });
                                    } else {
                                      this.widget.jumpFunction();
                                    }
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 15),
                                    child: Text('Next'),
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget imageContainer({String imagePath}) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(50),
        child: Image.asset(imagePath),
      ),
    );
  }

  Widget dotButton(BuildContext context,
      {@required presentPage, @required index}) {
    return index != presentPage
        ? Container(
            margin: EdgeInsets.only(left: 8, right: 8),
            height: 9,
            width: 9,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(1000),
                color: Theme.of(context).primaryColor.withOpacity(0.3)),
          )
        : Container(
            margin: EdgeInsets.only(left: 8, right: 8),
            height: 15,
            width: 15,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(1000),
                color: Theme.of(context).primaryColor),
          );
  }
}

class TextContainer extends StatefulWidget {
  String heading;
  String bodyText;
  TextContainer({Key key, @required this.heading, @required this.bodyText})
      : super(key: key);

  @override
  _TextContainerState createState() => _TextContainerState();
}

class _TextContainerState extends State<TextContainer> {
  double opacity = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      setState(() {
        this.opacity = 1;
      });
    });
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    print(' I am chnage w');
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: opacity,
      duration: Duration(milliseconds: 600),
      child: Padding(
        padding: EdgeInsets.all(mainPadding),
        child: Container(
          height: double.infinity,
          child: Column(
            children: [
              Text(
                this.widget.heading,
                style: Theme.of(context)
                    .textTheme
                    .headline4
                    .copyWith(fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Text(
                  this.widget.bodyText,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.subtitle2.copyWith(
                      fontWeight: FontWeight.w400, color: Color(0XFFA1A1A1)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
