import 'package:flutter/material.dart';

import '../utils.dart';

class DropDownNoBorder extends StatefulWidget {
  String value;
  Icon icon;
  List<String> items;
  Function onChanged;
  DropDownNoBorder(
      {@required this.value,
      this.icon,
      @required this.onChanged,
      @required this.items});

  @override
  _DropDownNoBorderState createState() => _DropDownNoBorderState();
}

class _DropDownNoBorderState extends State<DropDownNoBorder> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 46,
      decoration: BoxDecoration(
          color: Color(0XFFF5F5F5),
          borderRadius: BorderRadius.circular(mainBorder)),
      child: Padding(
        padding: const EdgeInsets.only(left: 16, right: 16),
        child: DropdownButton(
            onChanged: (String newValue) {
              widget.onChanged(newValue);
            },
            underline: Container(
              height: 0,
              decoration: BoxDecoration(color: Colors.transparent),
            ),
            isExpanded: true,
            value: widget.value,
            icon: widget.icon == null
                ? Icon(Icons.arrow_drop_down_outlined)
                : widget.icon,
            style: Theme.of(context)
                .textTheme
                .bodyText1
                .copyWith(color: Colors.black, fontWeight: FontWeight.w400),
            items: widget.items.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList()),
      ),
    );
  }
}
