import 'package:flutter/material.dart';

class MaterialChildButton extends StatelessWidget {
  Function onPress;
  Widget child;
  RoundedRectangleBorder shape;
  MaterialChildButton(
      {Key key, @required this.onPress, @required this.child, this.shape});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: this.shape,
      minWidth: 0,
      height: double.minPositive,
      padding: EdgeInsets.zero,
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      onPressed: () {
        this.onPress();
      },
      child: this.child,
    );
  }
}
