import 'package:flutter/material.dart';

import '../snippets.dart';

// ignore: non_constant_identifier_names
PreferredSize DrBar(BuildContext context,
    {Key key,
    hasBackButton,
    hasDrawer,
    @required title,
    centerTitle = true,
    GlobalKey<ScaffoldState> drawerKey}) {
  if (hasBackButton == null) {
    hasBackButton = false;
  }
  if (hasDrawer == null) {
    hasDrawer = false;
  }
  return PreferredSize(
    preferredSize: Size(double.infinity, 63),
    child: AppBar(
      elevation: 0,
      backgroundColor: Colors.white,
      leading: hasBackButton
          ? appBarBackButton(context)
          : hasDrawer
              ? appBarDrawerButton(context, drawerKey)
              : SizedBox(
                  height: 0,
                  width: 0,
                ),
      centerTitle: centerTitle,
      title: Text(
        title,
        style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontSize: 18,
            fontWeight: FontWeight.w600),
      ),
    ),
  );
}

Widget appBarBackButton(BuildContext context) {
  return Center(
    child: MaterialButton(
      minWidth: 0,
      padding: EdgeInsets.zero,
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      onPressed: () {
        pop(context);
      },
      child: Container(
        height: 35,
        width: 35,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Theme.of(context).primaryColor.withOpacity(.30),
        ),
        child: Center(
          child: Icon(
            Icons.arrow_back_rounded,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
    ),
  );
}

Widget appBarDrawerButton(
    BuildContext context, GlobalKey<ScaffoldState> drawerKey) {
  return Center(
    child: MaterialButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
      minWidth: 0,
      padding: EdgeInsets.zero,
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      onPressed: () {
        drawerKey.currentState.openDrawer();
      },
      child: Container(
        height: 35,
        width: 35,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          color: Theme.of(context).primaryColor.withOpacity(.30),
        ),
        child: Center(
          child: Icon(
            Icons.menu,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
    ),
  );
}
