import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../constants/drawerList.dart';

import '../snippets.dart';
import 'materialChildButton.dart';

class BookDrawer extends StatelessWidget {
  String activeScreen;
  BookDrawer({Key key, @required this.activeScreen}) : super(key: key);
  DrawerList drawerList = new DrawerList();
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: MediaQuery.of(context).size.width * 0.75,
      constraints: BoxConstraints(maxWidth: 500),
      decoration: BoxDecoration(color: Theme.of(context).primaryColor),
      child: Column(
        children: [
          Padding(
            padding:
                const EdgeInsets.only(left: 30, top: 30, right: 30, bottom: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Michael H.',
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w600,
                      color: Colors.white),
                ),
                Container(
                  height: 48,
                  width: 48,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assests/img/hair1.jpg')),
                      border: Border.all(color: Colors.white, width: 2),
                      borderRadius: BorderRadius.circular(50)),
                )
              ],
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(25),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10))),
              child: ListView(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  ...this.drawerList.drawerList.map((data) {
                    return MaterialChildButton(
                      onPress: () {
                        if (data.name != this.drawerList.logOut.name) {
                          popToMain(context);
                          pushNamed(context, data.routeName);
                        } else {}
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 40,
                          ),
                          Expanded(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Icon(
                                      data.icon,
                                      color: Theme.of(context).primaryColor,
                                      size: 21,
                                    ),
                                    SizedBox(
                                      width: 25,
                                    ),
                                    Text(
                                      data.name,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline6
                                          .copyWith(
                                              color: activeScreen == data.name
                                                  ? Theme.of(context)
                                                      .primaryColor
                                                  : Colors.black),
                                    ),
                                  ],
                                ),
                                Icon(
                                  Icons.arrow_forward_ios_outlined,
                                  color: Color(0XFFACABAB),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  }).toList()
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
