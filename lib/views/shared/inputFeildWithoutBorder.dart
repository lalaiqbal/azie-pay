import 'package:flutter/material.dart';

import '../utils.dart';

class InputNoBorder extends StatelessWidget {
  TextEditingController controller = new TextEditingController();
  final String hintText;
  IconData prefixIcon;
  bool obscureText = false;
  InputNoBorder(
      {Key key,
      this.controller,
      @required this.hintText,
      @required this.prefixIcon,
      obscureText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: TextFormField(
        obscureText: obscureText,
        controller: this.controller,
        style: TextStyle(
            color: Colors.black,
            fontSize: Theme.of(context).textTheme.subtitle2.fontSize),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(bottom: 11, top: 14),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(subBorder)),
              borderSide: BorderSide.none),
          hintText: this.hintText,
          prefixIcon: Icon(this.prefixIcon),
          filled: true,
          fillColor: Color(0xFFF5F5F5),
        ),
      ),
    );
  }
}
