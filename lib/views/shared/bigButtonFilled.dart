import 'package:flutter/material.dart';

import '../utils.dart';

class BigFilledButton extends StatefulWidget {
  final String buttonText;
  Function onPress;
  BigFilledButton({Key key, @required this.buttonText, @required this.onPress});

  @override
  _BigFilledButtonState createState() => _BigFilledButtonState();
}

class _BigFilledButtonState extends State<BigFilledButton> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 50,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
              side: BorderSide(width: 1, color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(subBorder)),
          elevation: 0,
          color: Theme.of(context).primaryColor,
          onPressed: () {
            widget.onPress();
          },
          child: Center(
            child: Text(this.widget.buttonText,
                style: Theme.of(context).textTheme.bodyText1.copyWith(
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    )),
          ),
        ));
  }
}
