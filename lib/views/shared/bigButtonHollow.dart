import 'package:flutter/material.dart';

import '../utils.dart';

class BigHollowButton extends StatefulWidget {
  final String buttonText;
  Color bgColor;
  Function onPress;
  BigHollowButton(
      {Key key,
      @required this.buttonText,
      @required this.onPress,
      this.bgColor});

  @override
  _BigHollowButtonState createState() => _BigHollowButtonState();
}

class _BigHollowButtonState extends State<BigHollowButton> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
            side: BorderSide(width: 1, color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(mainBorder)),
        elevation: 0,
        color: this.widget.bgColor == null ? Colors.white : this.widget.bgColor,
        onPressed: () {
          widget.onPress();
        },
        child: Center(
            child: Text(
          this.widget.buttonText,
          style: Theme.of(context).textTheme.bodyText1.copyWith(
              fontWeight: FontWeight.w600,
              color: Theme.of(context).primaryColor),
        )),
      ),
    );
  }
}
