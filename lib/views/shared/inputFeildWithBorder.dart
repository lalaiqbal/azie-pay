import 'package:flutter/material.dart';

import '../utils.dart';

class InputWithBorder extends StatelessWidget {
  Color fillColor;
  TextEditingController controller = new TextEditingController();
  final String hintText;
  Icon prefixIcon;
  InputWithBorder(
      {Key key,
      this.controller,
      this.hintText,
      this.prefixIcon,
      this.fillColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 46,
      child: TextFormField(
        controller: this.controller,
        style: TextStyle(color: Colors.black),
        decoration: InputDecoration(
            labelText: this.hintText,
            contentPadding: EdgeInsets.only(bottom: 11, top: 14),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(mainBorder)),
                borderSide: BorderSide(
                    width: 1, color: Theme.of(context).primaryColor)),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(mainBorder)),
                borderSide: BorderSide(
                    width: 1, color: Theme.of(context).primaryColor)),
            prefixIcon: this.prefixIcon,
            filled: true,
            fillColor:
                this.fillColor == null ? Color(0xFFFFFFFF) : this.fillColor),
      ),
    );
  }
}
