import 'package:flutter/material.dart';

import '../utils.dart';

class MediumButtonFilled extends StatelessWidget {
  final String buttonText;
  final Function onPress;
  const MediumButtonFilled({@required this.onPress, @required this.buttonText});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 37,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
              side: BorderSide(width: 1, color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(subBorder)),
          elevation: 0,
          color: Theme.of(context).primaryColor,
          onPressed: () {
            this.onPress();
          },
          child: Center(
            child: Text(
              this.buttonText,
              style: Theme.of(context)
                  .textTheme
                  .subtitle2
                  .copyWith(fontWeight: FontWeight.w600, color: Colors.white),
            ),
          ),
        ));
  }
}
