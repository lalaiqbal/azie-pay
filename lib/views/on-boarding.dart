import 'package:azie_pay/views/on-boarding-child1.dart';
import 'package:azie_pay/views/on-boarding-child2.dart';
import 'package:flutter/material.dart';

class OnBoarding extends StatefulWidget {
  OnBoarding({Key key}) : super(key: key);

  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  Widget child;
  double _opacity = 0;
  void _jumpFunction() {
    setState(() {
      this.child = OnBoarding2();
      this._opacity = 1;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.child = OnBoarding1(
      jumpFunction: _jumpFunction,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: AnimatedSwitcher(
        switchInCurve: Curves.easeOutCirc,
        duration: Duration(seconds: 3),
        child: child,
        transitionBuilder: (
          Widget child,
          Animation<double> animation,
        ) {
          return SizeTransition(
            child: child,
            sizeFactor: animation,
            axisAlignment: 3,
            axis: Axis.vertical,
          );
        },
      ),
    );
  }
}
