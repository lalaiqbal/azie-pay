import 'package:flutter/material.dart';

// import 'package:intl/intl.dart';
// import 'package:url_launcher/url_launcher.dart';

// DateFormat get timeFormat => DateFormat.jm();
//
// DateFormat get dateFormat => DateFormat.yMMMMd();

String Function(String) get mandatoryValidator =>
    (val) => val.isEmpty ? "This field is mandatory" : null;
RoundedRectangleBorder getRoundShape() =>
    RoundedRectangleBorder(borderRadius: getRoundBorder());

BorderRadius getRoundBorder() => BorderRadius.circular(16);

String Function(String) get emailValidator => (email) => RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+",
    ).hasMatch(email)
        ? null
        : "Enter a valid email";

String Function(String) get passwordValidator =>
    (String password) => password.length < 8 ? "Password too short" : null;

Future<T> push<T>(BuildContext context, Widget child) =>
    Navigator.of(context).push<T>(MaterialPageRoute(builder: (_) => child));

void replace(BuildContext context, Widget child) => Navigator.of(context)
    .pushReplacement(MaterialPageRoute(builder: (_) => child));

void pop(BuildContext context) => Navigator.of(context).pop();
void showDialogOf(context, Widget child) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return child;
      });
}

void popToMain(BuildContext context) =>
    Navigator.of(context).popUntil((route) => route.isFirst);
void pushNamed(BuildContext context, String routeName) =>
    Navigator.pushNamed(context, routeName);

void snack(BuildContext context, String message, {bool info = false}) =>
    Scaffold.of(context).showSnackBar(SnackBar(
      backgroundColor: info ? Colors.green : Colors.red,
      content: Text(
        message,
        style: Theme.of(context).textTheme.bodyText1.copyWith(
              color: Colors.white,
            ),
      ),
    ));
Widget star(double value, BuildContext context, {bool reduceSide = false}) {
  if (value <= 2) {
    return Icon(
      Icons.star_outline,
      color: Theme.of(context).primaryColor,
      size: reduceSide ? 10 : 24,
    );
  }
  if (value > 2 && value < 4.1) {
    return Icon(Icons.star_half_outlined,
        color: Theme.of(context).primaryColor, size: reduceSide ? 10 : 24);
  }
  if (value >= 4.1) {
    return Icon(Icons.star_outlined,
        color: Theme.of(context).primaryColor, size: reduceSide ? 10 : 24);
  }
  return Icon(Icons.star_outline,
      color: Theme.of(context).primaryColor, size: reduceSide ? 10 : 24);
}
// Future<UserData> get loadUserData async => UserData(
//       image: await prefsImageUrl.load(),
//       fullName: await prefsFullName.load(),
//       email: await prefsEmail.load(),
//     );
//
// Future<String> readToken(BuildContext context) =>
//     context.read<AuthState>().token;
//
// Future<String> watchToken(BuildContext context) =>
//     context.watch<AuthState>().token;

// Future<bool> openMap(
//     {double latitude = -3.823216, double longitude = -38.481700}) async {
//   String googleUrl =
//       'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
//   if (await canLaunch(googleUrl)) {
//     await launch(googleUrl);
//     return true;
//   } else {
//     return false;
//   }
// }

Widget getFormFieldError(FormFieldState<String> state) => state.hasError
    ? Builder(
        builder: (context) => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Divider(color: Colors.red.shade700),
            Text(state.errorText,
                style: Theme.of(context)
                    .textTheme
                    .caption
                    .copyWith(color: Colors.red.shade700)),
          ],
        ),
      )
    : SizedBox();
